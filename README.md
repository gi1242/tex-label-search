# Tex Label Search

Remaps `*` and `#` for `tex` files so that when the cursor is on a label, they
will search forward / backward for the whole label (for instance
`\ref{e:long-equation-label}`). Labels can't have spaces in them. Outside
labels, the usual `#`, `*` behavior is retained.
