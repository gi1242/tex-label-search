-- Map # and * so that they search for labels when inside {..}
if vim.b.did_ftplugin_tex_label_search == 1 then
  return
end
vim.b.did_ftplugin_tex_label_search = 1
-- print( 'loaded tex-label-search' )

local function label_search( key )
  local col = vim.api.nvim_win_get_cursor(0)[2] + 1
  local line = vim.api.nvim_get_current_line()
  -- print( line )
  for mstart, word, mend in line:gmatch( '\\%w+(){([%w:_%-]+)}()' ) do
    if mstart <= col and col < mend then
      print( 'matched ', word )
      return (key == '#' and '?' or '/')
	.. '{\\zs' .. word .. '\\ze}<cr>'
    end
  end
  print( 'nomatch' )
  return key
end

for _, v in pairs( {'#', '*'} ) do
  vim.keymap.set( 'n', v,
    function () return label_search( v ) end,
    {expr=true, buffer=true} )
end
